// TODO(you): Modify the class in whatever ways necessary to implement
// the flashcard app behavior.
//
// You may need to do things such as:
// - Changing the constructor parameters
// - Adding methods
// - Adding additional fields

class ResultsScreen {
  constructor(containerElement) {
    this.containerElement = containerElement;
  }

  show(numberCorrect, numberWrong) {
  	const per = this.containerElement.querySelector(".percent");
  	per.textContent = Math.floor(numberCorrect / total * 100);
  	const correct = this.containerElement.querySelector(".correct");
  	correct.textContent = numberCorrect;
  	const incorrect = this.containerElement.querySelector(".incorrect");
  	incorrect.textContent = numberWrong;
	this.containerElement.classList.remove('inactive');
  }

  hide() {
    this.containerElement.classList.add('inactive');
  }
}

function menu(event){
	i = 1;
	total = 0;
	wrong = 0;
	correct = 0;
	app.flashcards.correct.textContent = correct;
	app.flashcards.wrong.textContent = wrong;
	app.results.hide();
	app.menu.show();
}
    
// app.menu.show();
const tomenu = /*app.results.containerElement*/ document.querySelector(".to-menu");
tomenu.addEventListener('click',menu);
