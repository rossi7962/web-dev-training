
var done1 = 0;
var done2 = 0;
var done3 = 0;

var v1;
var v2;
var v3;


const container = document.querySelector('#final');

function restart_quiz(){

	for (const box of boxes1) {
		box.classList.remove('selected');
		const check = box.querySelector('.checkbox');
		check.src = './images/unchecked.png';
		box.addEventListener('click', choice1);
	}


	for (const box of boxes2) {
		box.classList.remove('selected');
		const check = box.querySelector('.checkbox');
		check.src = './images/unchecked.png';
		box.addEventListener('click', choice2);
	}


	for (const box of boxes3) {
		box.classList.remove('selected');
		const check = box.querySelector('.checkbox');
		check.src = './images/unchecked.png';
		box.addEventListener('click', choice3);
	}

	var element = container.querySelector('h1');
	element.parentNode.removeChild(element);
	element = container.querySelector('div');
	element.parentNode.removeChild(element);
	element = container.querySelector('button');
	element.parentNode.removeChild(element);
	//container.classList.add('#final');
	container.style.display = 'none';
	done1 = 0;
	done2 = 0;
	done3 = 0;
}

function choice1(event) {
	for (const box of boxes1) {
		box.classList.remove('selected');
		const check = box.querySelector('.checkbox');
		check.src = './images/unchecked.png';
	}
	const container = event.currentTarget;
	container.classList.add('selected');

	const check = event.currentTarget.querySelector('.checkbox');
	check.src = './images/checked.png';
	done1 = 1;
	v1 = event.currentTarget.getAttribute('data-choice-id');

	// for(i1 = 0; i1 < 9; i1 += 1 )
	// 	if(RESULTS_MAP[i1] === op)
	// 		break;
	// console.log(RESULTS_MAP[i1]);
	if( isCompleted() )
	{

		result();
	}
}


const boxes1 = document.querySelectorAll('div[data-question-id="one"]');

for (const box of boxes1) {
	box.addEventListener('click', choice1);
}

function choice2(event) {
	for (const box of boxes2) {
		box.classList.remove('selected');
		const check = box.querySelector('.checkbox');
		check.src = './images/unchecked.png';
	}
	const container = event.currentTarget;
	container.classList.add('selected');

	const check = event.currentTarget.querySelector('.checkbox');
	check.src = './images/checked.png';
	done2 = 1;
	// id2 = event.currentTarget.getAttribute('data-choice-id');
	
	v2 = event.currentTarget.getAttribute('data-choice-id');
	if( isCompleted() )
	{
		result();
	}
}


const boxes2 = document.querySelectorAll('div[data-question-id="two"]');

for (const box of boxes2) {
	box.addEventListener('click', choice2);
}

function choice3(event) {
	for (const box of boxes3) {
		box.classList.remove('selected');
		const check = box.querySelector('.checkbox');
		check.src = './images/unchecked.png';
	}
	const container = event.currentTarget;
	container.classList.add('selected');

	const check = event.currentTarget.querySelector('.checkbox');
	check.src = './images/checked.png';
	done3 = 1;
	// id3 = event.currentTarget.getAttribute('data-choice-id');
	
	v3 = event.currentTarget.getAttribute('data-choice-id');
	if( isCompleted() )
	{
		result();
	}
}


const boxes3 = document.querySelectorAll('div[data-question-id="three"]');

for (const box of boxes3) {
	box.addEventListener('click', choice3);
}


function isCompleted(){
	return (done1 + done2 + done3 === 3);
}

function on_hover(){
	const hov = event.currentTarget;
	hov.style.cssText = 'background-color: #e0e0e0;text-align : center; 	margin-bottom: 20px;	height: 50px;	margin-left: 20px;	margin-right: 20px;';
}

function on_out(){
	const hov = event.currentTarget;
	hov.style.cssText = 'background-color: #cecece;text-align : center; 	margin-bottom: 20px;	height: 50px;	margin-left: 20px;	margin-right: 20px;';
}


function judge(v1,v2,v3){
	if(v1 === v2 && v2 === v3)
		return v1;
	else if(v1 === v2 || v1 === v3)
		return v1;
	else if(v2 === v1 || v2 === v3)
		return v2;
	else if(v3 === v1 || v3 === v2)
		return v3;
	else	
		return v1;

}

function result(){
	




	var trait = judge(v1,v2,v3);

	container.classList.add('result');
	container.style.display = 'flex' ;
	const header = document.createElement('h1');
	header.textContent = 'You got: ' + RESULTS_MAP[trait].title;
	container.appendChild(header);

	const info = document.createElement('div');
	info.textContent = RESULTS_MAP[trait].contents;
	info.style.cssText = 'text-align : justify; padding : 20px';
	container.appendChild(info);

	const button = document.createElement('button');
	button.textContent = 'Restart Quiz!';
	button.style.cssText = 'background-color: #cecece;text-align : center; 	margin-bottom: 20px;	height: 50px;	margin-left: 20px;	margin-right: 20px;';
	container.appendChild(button);

	for (const box of boxes1) {
		box.removeEventListener('click', choice1);
	}
	for (const box of boxes2) {
		box.removeEventListener('click', choice2);
	}
	for (const box of boxes3) {
		box.removeEventListener('click', choice3);
	}

	button.addEventListener('mouseover',on_hover);
	button.addEventListener('mouseout',on_out);
	button.addEventListener('click',restart_quiz);
}


