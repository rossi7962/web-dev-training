const express = require('express');
const bodyParser = require('body-parser');
const googleSheets = require('gsa-sheets');

const key = require('./privateSettings.json');

// TODO(you): Change the value of this string to the spreadsheet id for your
// GSA spreadsheet. See HW5 spec for more information.
const SPREADSHEET_ID = '1yFLlzIBW63K-rKxuPxIPsi14g01Twqy_VhMeQhLRq-A';
const link = 'https://docs.google.com/spreadsheets/d/1yFLlzIBW63K-rKxuPxIPsi14g01Twqy_VhMeQhLRq-A';
const app = express();
const jsonParser = bodyParser.json();
const sheet = googleSheets(key.client_email, key.private_key, SPREADSHEET_ID);

app.use(express.static('public'));

async function onGet(req, res) {
  const result = await sheet.getRows();
  const rows = result.rows;
  // TODO(you): Finish onGet.
  const name = rows[0][0];
  console.log(name);
  const email = rows[0][1];

  console.log(email);
  var i = 0;

  var response = [];
  for(const row of rows)
  {
  	if(i === 0) 
  	{
  		i += 1;

  		continue;
  	}
  	var person = {name: row[0],email: row[1]};
  	response.push(person);
  }
  res.json(response);
}
app.get('/api', onGet);

async function onPost(req, res) {
  const messageBody = req.body;
  //name = messageBody.name;
  //email = messageBody.email;
  for(const obj in messageBody)
  {
  	console.log(obj);
  	console.log(messageBody[obj]);
  	sheet.appendRow([obj,messageBody[obj]]);
  }
  //sheet.appendRow(['kaka','kaka@gmail.com']);
  // TODO(you): Implement onPost.
  res.json({"response":"success"});
}
app.post('/api', jsonParser, onPost);

async function onPatch(req, res) {
  const column  = req.params.column;
  const value  = req.params.value;
  const messageBody = req.body;

  // TODO(you): Implement onPatch.
  const result = await sheet.getRows();
  const rows = result.rows;
  let i = 0;
  let j = 0;
  //console.log(value);
  for(const row of rows)
  {
  	//console.log(row);
  	if(i === 0)
  	{
  		i += 1;
  		if(column === 'name')
  			j = 0;
  		if(column === 'email')
  			j = 1;
  		continue;
  		//continue;
  	}
  	var object = row[j];
  	console.log(object);
  	if(value === object)
  	{
  		console.log('mitsuketa!');
  		//sheet.deleteRow(i);
  		for(const obj in messageBody)
  		{
  			console.log(obj);
  			console.log(messageBody[obj]);
  			sheet.setRow(i,[obj,messageBody[obj]]);
  		}
  		res.json({response: "success"});
  	}
  	i += 1;

  }
  //res.json( { status: 'unimplemented'} );
}
app.patch('/api/:column/:value', jsonParser, onPatch);

async function onDelete(req, res) {
  const column  = req.params.column;
  const value  = req.params.value;
  const result = await sheet.getRows();
  const rows = result.rows;
  let i = 0;
  let j = 0;
  //console.log(value);
  for(const row of rows)
  {
  	//console.log(row);
  	if(i === 0)
  	{
  		i += 1;
  		if(column === 'name')
  			j = 0;
  		if(column === 'email')
  			j = 1;
  		continue;
  	}
  	var object = row[j];
  	console.log(object);
  	if(value === object)
  	{
  		console.log('mitsuketa!');
  		sheet.deleteRow(i);
  		res.json({response: "success"});
  	}
  	i += 1;

  }
  // TODO(you): Implement onDelete.

  res.json( { status: 'notthing to delete'} );
}
//app.delete('/api/:column/:value',  onDelete);
app.delete('/api/:column/:value',  onDelete);


// Please don't change this; this is needed to deploy on Heroku.
const port = process.env.PORT || 3000;

app.listen(port, function () {
  console.log('CS193X: Server listening on port ' + port + ' !' /*${port}!'*/);
});
