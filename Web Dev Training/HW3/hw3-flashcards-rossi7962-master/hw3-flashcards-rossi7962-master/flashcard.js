// TODO(you): Modify the class in whatever ways necessary to implement
// the flashcard app behavior.
//
// You may need to do things such as:
// - Changing the constructor parameters
// - Adding methods
// - Adding additional fields
let originX = null;
let originY = null;
let offsetX = 0;
let offsetY = 0;
let dragStarted = false;
let moved = false;
let i = 1;
//let choose = false;
class Flashcard {
  constructor(containerElement, frontText, backText) {
    this.back = document.querySelector('body');
    this.containerElement = containerElement;

    this._flipCard = this._flipCard.bind(this);
    this._onDragStart = this._onDragStart.bind(this);
    this._onDragEnd = this._onDragEnd.bind(this);
    this._onDragMove = this._onDragMove.bind(this);
   

    this.flashcardElement = this._createFlashcardDOM(frontText, backText);
    this.containerElement.append(this.flashcardElement);

    this.flashcardElement.addEventListener('click', this._flipCard);
    //this.flashcardElement.addEventListener('', this.);
    this.flashcardElement.addEventListener('pointerdown', this._onDragStart);
    this.flashcardElement.addEventListener('pointerup', this._onDragEnd);
    this.flashcardElement.addEventListener('pointermove', this._onDragMove);
  }

  // Creates the DOM object representing a flashcard with the given
  // |frontText| and |backText| strings to display on the front and
  // back of the card. Returns a reference to root of this DOM
  // snippet. Does not attach this to the page.
  //
  // More specifically, this creates the following HTML snippet in JS
  // as a DOM object:
  // <div class="flashcard-box show-word">
  //   <div class="flashcard word">frontText</div>
  //   <div class="flashcard definition">backText</div>
  // </div>
  // and returns a reference to the root of that snippet, i.e. the
  // <div class="flashcard-box">
  _createFlashcardDOM(frontText, backText) {
    const cardContainer = document.createElement('div');
    cardContainer.classList.add('flashcard-box');
    cardContainer.classList.add('show-word');

    const wordSide = document.createElement('div');
    wordSide.classList.add('flashcard');
    wordSide.classList.add('word');
    wordSide.textContent = frontText;

    const definitionSide = document.createElement('div');
    definitionSide.classList.add('flashcard');
    definitionSide.classList.add('definition');
    definitionSide.textContent= backText;

    cardContainer.appendChild(wordSide);
    cardContainer.appendChild(definitionSide);
    return cardContainer;
  }


  _onDragStart(event) {
    event.currentTarget.style.transition = '0s';
    originX = event.clientX;
    originY = event.clientY;
    dragStarted = true;
    event.currentTarget.setPointerCapture(event.pointerId);
  }

  _onDragMove(event) {
    if (!dragStarted) {
      return;
    }
    event.preventDefault();
    const deltaX = event.clientX - originX;
    const deltaY = event.clientY - originY;
    const translateX = offsetX + deltaX;
    const translateY = offsetY + deltaY;
    event.currentTarget.style.transform = 'translate(' + 
    translateX + 'px, ' + translateY + 'px)rotate(' + 0.2 * deltaX + 'deg)';
    
    if(deltaX >= 150 || deltaX <= -150)
    {
      this.back.style.cssText = "background-color : #97b7b7;";
    }
    else
    {
      this.back.style.cssText = "background-color : #d0e6df;";
    }

  }

  _onDragEnd(event) {
    dragStarted = false;  
    offsetX =  event.clientX - originX;
    offsetY =  event.clientY - originY;
    if(offsetX === 0 && offsetY === 0 )
      moved = false;
    else
      moved = true;
    if(offsetX <= -150 || offsetX >= 150)
    {
      if(offsetX <= -150) 
      {
        wrong += 1;
        app.flashcards.wrong.textContent = wrong;
      }
      if(offsetX >= 150)
      {
        correct += 1;
        app.flashcards.correct.textContent = correct;
      }
      i += 1;
      var element = event.currentTarget;
      element.parentNode.removeChild(element);
      //choose = true;
      //const card = new Flashcard(flashcardContainer, 'Rin <3', 'Len <3');
      const flashcardContainer = document.querySelector('#flashcard-container');
      let j = 0;
      for(const mem in FLASHCARD_DECKS[deck].words)
      {
        j += 1;
        if( i === j )
        {
          const card = new Flashcard(flashcardContainer,mem,FLASHCARD_DECKS[deck].words[mem]);
          break;
        }
      }
      if( i === total + 1)
      {
        app.flashcards.hide();
        app.results.show(correct,wrong);
        i = 1;
      }

    }
    else
    {
      event.currentTarget.style.transition = '0.3s';
      event.currentTarget.style.transform = 'translate(0px,0px)';

    }
    offsetX = 0;
    offsetY = 0;
    this.back.style.cssText = "background-color : #d0e6df;";
  } 
  

  _flipCard(event) {
    if(!moved)
    {
      this.flashcardElement.classList.toggle('show-word');
    }
  }
}
