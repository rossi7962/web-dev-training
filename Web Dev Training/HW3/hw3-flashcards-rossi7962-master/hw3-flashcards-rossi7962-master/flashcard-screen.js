// TODO(you): Modify the class in whatever ways necessary to implement
// the flashcard app behavior.
//
// You may need to do things such as:
// - Changing the constructor parameters
// - Rewriting some of the existing methods, such as changing code in `show()`
// - Adding methods
// - Adding additional fields

class FlashcardScreen {
  constructor(containerElement) {
    this.containerElement = containerElement;
    this.wrong = this.containerElement.querySelector('.incorrect');
    this.correct = this.containerElement.querySelector('.correct');
  }
  

  show() {
    this.containerElement.classList.remove('inactive');

    const flashcardContainer = document.querySelector('#flashcard-container');
    
    //const card = new Flashcard(flashcardContainer, 'Rin <3', 'Len <3');
    //const card2 = new Flashcard(flashcardContainer, 'Vocal 01 <3', 'Vocal 02 <3');
  }

  hide() {
    this.containerElement.classList.add('inactive');
  }
}
