// This class will represent the menu screen that you see when you first load
// the music visualizer.
//
// See HW4 writeup for more hints and details.
const pick = [];
var my_data;
class MenuScreen {
	constructor(container) {
		this.container = container;
		this.show = this.show.bind(this);
		this.hide = this.hide.bind(this);
		// this.song = document.getElementById("song-selector");
		// this.theme = document.getElementById("query-input");
		// this.load = this.load.bind(this);

	}

	show() {
		this.container.classList.remove('inactive');

	}

	hide() {
		this.container.classList.add('inactive');
	}
	load()
	{
		fetch("https://yayinternet.github.io/hw4-music/songs.json")
		.then(this._onResponse)
		.then(this._onJsonReady); 
	}

	_onJsonReady(json) 
	{
		my_data = json;
		for(const info in json)
		{
			const container = document.getElementById("song-selector");
			var option = document.createElement('option');
			pick.push(info);
			option.textContent = json[info].artist + ": " + json[info].title;
			container.appendChild(option);
		}
	}




	_onResponse(response) {
		return response.json();
	}
	option(obj){
		const container = document.getElementById("query-input");
		var i = Math.floor(Math.random() * 10);
		container.value = obj[i];
	}

}

var song_name;
var song_artist;
var song_url;
const theme = ['candy', 
'charlie brown', 'computers', 
'dance', 'donuts', 'hello kitty', 
'flowers', 'nature', 
'turtles', 'space'];

// const menuElement = document.querySelector('#menu');
// menu = new MenuScreen(menuElement);
// menu.load();
// menu.option(theme);
console.log(pick);
function onSubmit(event){
	event.preventDefault();
	app.menu.hide();
	app.box.show();
	const container = document.getElementById("song-selector");
	app.player.setSong(my_data[pick[container.selectedIndex]].songUrl);
	const background = document.getElementById("query-input");
	console.log('Submitted: ' + my_data[pick[container.selectedIndex]].songUrl);
	//const gif = app.box.container.querySelector('#gif-display');
	//console.log(encodeURIComponent(text));
	//gif.style.backgroundImage = "url('https://media3.giphy.com/media/l3vR6JC7NZZgTpmWQ/giphy.gif')";

	function onTextReady(json) {
		const gif = app.box.container.querySelector('#gif-display');
		const gifUrl = json.data[0].images.downsized.url;
		console.log(json.data);

		gif.style.backgroundImage = "url(" + gifUrl + ")";
	}
	function onResponse(response) {
		return response.json();
	}
	//fetch('https://yayinternet.github.io/lecture17/oo-albums/albums.json')
	fetch('http://api.giphy.com/v1/gifs/search?q=' 
		+ background.value 
		+ '&api_key=Q4NbXMllRPTYWn6ZIj5a1iiszIqDRE9X')
	.then(onResponse)
	.then(onTextReady);
}

function onKeyPress(event){
	//console.log(event.key);
	if( event.key === 'Enter')
		onSubmit(event);
}


document.addEventListener('submit',onSubmit);
document.addEventListener('keypress',onKeyPress);