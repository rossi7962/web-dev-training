// TODO(you): Modify the class in whatever ways necessary to implement
// the flashcard app behavior.
//
// You may need to do things such as:
// - Changing the constructor parameters
// - Adding methods
// - Adding additional fields

let string = '';
let deck = 0;
let total = 0;
let wrong = 0;
let correct = 0;
class MenuScreen {
  constructor(containerElement) {
    this.containerElement = containerElement;
    //this.OnClick() = this.OnClick().bind(this);
    const choices = document.getElementById('choices');

  }


  show() {
    this.containerElement.classList.remove('inactive');

  }

  hide() {
    this.containerElement.classList.add('inactive');
  }


}

function OnClick(event)
{
	deck = 0;
	string = event.currentTarget.textContent;
	//console.log(string);
	for(const mem of FLASHCARD_DECKS)
	{
		console.log(mem.title);
		if( mem.title === string)
		{

			break;
		}
		deck += 1;
	}
	app.menu.hide();

	for(const mem in FLASHCARD_DECKS[deck].words)
	{
		if(total === 0)
		{
			const flashcardContainer = document.querySelector('#flashcard-container');
			new Flashcard(flashcardContainer, mem,FLASHCARD_DECKS[deck].words[mem] );
		}
		total += 1;
	}
	app.flashcards.show();
}

for(const item of FLASHCARD_DECKS)
{
	console.log(item.title);
	const op = document.createElement('div');
	op.textContent = item.title;
	choices.appendChild(op);
	op.addEventListener('click',OnClick);
}

