// This class will represent the music visualizer as a whole, similar to the
// role that the `App` class played in HW3.
//
// See HW4 writeup for more hints and details.
class App {
	constructor() {
		const menuElement = document.querySelector('#menu');
		this.menu = new MenuScreen(menuElement);

		const boxElement = document.querySelector('#music-screen');
		this.box = new MusicScreen(boxElement);

		this.player = new AudioPlayer();
	}

}

app = new App();
app.menu.load();
app.menu.option(theme);

function onplay(event){

	app.player.play();
	play.classList.add('inactive');
	pause.classList.remove('inactive');
}

function onpause(event){

	app.player.pause();
	play.classList.remove('inactive');
	pause.classList.add('inactive');	
}
const play = app.box.container.querySelector('#play-button');
play.addEventListener('click',onplay);
const pause = app.box.container.querySelector('#pause-button');
pause.addEventListener('click',onpause);

